package app.storage;


import lombok.AllArgsConstructor;
import lombok.Data;

import java.util.HashMap;
import java.util.Map;

@Data
@AllArgsConstructor
public class InMemoryStorage<K, V> implements PersistentStorage<K, V> {
    private Map<K, V> map;

    public InMemoryStorage(InMemoryStorage<K,V> storage)
    {
        this();

        for(K key :storage.getMap().keySet())
        {
            map.put(key,map.get(key));
        }
    }

    public InMemoryStorage() {
        map = new HashMap<>();
    }

    @Override
    public void set(K key, V value) {
        map.put(key,value);
    }

    @Override
    public V get(K key) {
        return map.get(key);
    }

    @Override
    public boolean exists(K key) {
        return map.containsKey(key);
    }

    @Override
    public void delete(K key) {
        map.remove(key);
    }
}
