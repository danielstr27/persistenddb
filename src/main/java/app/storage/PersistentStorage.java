package app.storage;

public interface PersistentStorage<K,V> {
    void set(K key, V value);

    V get(K key);

    boolean exists(K key);

    void delete(K key);
}
