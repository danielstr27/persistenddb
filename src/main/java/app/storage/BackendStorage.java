package app.storage;

public class BackendStorage<K, V> implements PersistentStorage<K, V> {

    public BackendStorage(BackendStorage<K, V> storage) {
        // Stub
    }

    public BackendStorage() {
        // Stub
    }

    @Override
    public void set(K key, V value) {
        // Stub
    }

    @Override
    public V get(K key) {
        // Stub
        return null;
    }

    @Override
    public boolean exists(K key) {
        // Stub
        return false;
    }

    @Override
    public void delete(K key) {
        // Stub
    }
}
