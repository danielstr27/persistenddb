package app;

import app.bl.DataAccessLayer;
import app.bl.TransactionalDAL;
import app.exceptions.StorageException;
import app.exceptions.TransactionException;
import app.storage.InMemoryStorage;

import java.util.Scanner;

public class Main {

    public static final String IN_MEMORY_TRANSACTIONAL_MODE = "IT";
    private static final String BACKEND_MODE = "B";

    // NOT SUPPORTED
//    public static final String IN_MEMORY_MODE = "I";
//    public static final String BACKEND_TRANSACTIONAL_MODE = "BT";

    public static void main(String[] args) {
        printBanner();
        DataAccessLayer<String, String> dataAccessLayer;
        while (true) {
            System.out.println(String.format("Please select what mode to initialize(  %s | %s):",  IN_MEMORY_TRANSACTIONAL_MODE, BACKEND_MODE));
            String consoleInput = getConsoleInput();
            if (consoleInput.equals(IN_MEMORY_TRANSACTIONAL_MODE)) {
                dataAccessLayer = new TransactionalDAL<>(new InMemoryStorage<>());
            }
            // Just for ease
            else break;

            try {
                // EXAMPLE
                dataAccessLayer.insert("i", "2");
                ((TransactionalDAL<String,String>)dataAccessLayer).beginTransaction();
                dataAccessLayer.update("i", "5");
                ((TransactionalDAL<String, String>) dataAccessLayer).commit();
                dataAccessLayer.insert("j", "10");
                dataAccessLayer.delete("j");

                System.out.println();
            } catch (StorageException | TransactionException e) {
                e.printStackTrace();
            }
        }
    }

    private static String getConsoleInput() {
        Scanner input = new Scanner(System.in);
        return input.nextLine();
    }


    private static void printBanner() {
        System.out.println("             _");
        System.out.println(" __ __ _____| |__ ___ _ __  ___ ");
        System.out.println(" \\ V  V / -_) / _/ _ \\ '  \\/ -_)");
        System.out.println("  \\_/\\_/\\___|_\\__\\___/_|_|_\\___|");
    }
}
