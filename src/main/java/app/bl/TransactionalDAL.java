package app.bl;

import app.exceptions.TransactionException;
import app.storage.PersistentStorage;

import java.util.Optional;

public class TransactionalDAL<K, V> extends DataAccessLayer<K, V> {
    private Transaction<K, V> transaction;
    private SnapshotHandler<K, V> snapshotHandler;
    private boolean duringTransaction;

    public TransactionalDAL(PersistentStorage<K, V> storage, SnapshotHandler<K, V> snapshotHandler) {
        super(snapshotHandler.getSnapshot());
        duringTransaction = false;
    }

    protected PersistentStorage<K, V> getRelevantStorage() {
        return duringTransaction ? transaction.getSnapshot() : (PersistentStorage<K, V>) storage;
    }

    public void toggleTransaction() throws TransactionException {
        if (Optional.ofNullable(transaction).isPresent()) {
            duringTransaction = !duringTransaction;
        } else {
            duringTransaction = false;
            throw new TransactionException("No transaction is currently in progress");
        }
    }

    public void beginTransaction() throws TransactionException {
        if (Optional.ofNullable(transaction).isPresent()) {
            throw new TransactionException("Only one transaction at a time is supported");
        } else {
            transaction = new Transaction<K, V>(storage);
            duringTransaction = true;
        }
    }

    // For security reasons, I didn't allow to make commit if transaction toggle wasn't handled properly by the user
    public void commit() throws TransactionException {
        if (!Optional.ofNullable(transaction).isPresent()) {
            throw new TransactionException("No transaction is currently in progress");
        }

        if (!duringTransaction) {
            throw new TransactionException("Current transaction is frozen");
        } else {
            this.storage = transaction.getSnapshot();

            duringTransaction = false;
            transaction = null;
        }
    }

    public void rollback() throws TransactionException {
        if (!Optional.ofNullable(transaction).isPresent()) {
            throw new TransactionException("No transaction is currently in progress");
        }

        duringTransaction = false;
        transaction = null;
    }
}
