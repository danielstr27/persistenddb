package app.bl;

import app.exceptions.StorageException;
import app.storage.PersistentStorage;
import lombok.Data;

@Data
public abstract class DataAccessLayer<K, V> {
    protected PersistentStorage<K, V> storage;

    public DataAccessLayer(PersistentStorage<K, V> storage) {
        this.storage = storage;
    }

    public void insert(K key, V value) throws StorageException {
        PersistentStorage<K, V> workingStorage = getRelevantStorage();
        if (workingStorage.exists(key)) {
            throw new StorageException("Key already exists in storage");
        } else
            workingStorage.set(key, value);
    }

    public void update(K key, V value) throws StorageException {
        PersistentStorage<K, V> workingStorage = getRelevantStorage();
        if (!workingStorage.exists(key)) {
            throw new StorageException("Key doesn't exists in storage");
        } else
            workingStorage.set(key, value);
    }

    public void upsert(K key, V value) {
        PersistentStorage<K, V> workingStorage = getRelevantStorage();
        workingStorage.set(key, value);
    }

    public void delete(K key) {
        PersistentStorage<K, V> workingStorage = getRelevantStorage();
        workingStorage.delete(key);
    }

    public V select(K key) throws StorageException {
        PersistentStorage<K, V> workingStorage = getRelevantStorage();
        if (!workingStorage.exists(key)) {
            throw new StorageException("Key doesn't exists in storage");
        }

        return workingStorage.get(key);
    }

    protected abstract PersistentStorage<K, V> getRelevantStorage();
}
