package app.bl;

import app.exceptions.StorageException;
import app.storage.PersistentStorage;

public interface SnapshotHandler<K, V> {
    PersistentStorage<K,V> getSnapshot(PersistentStorage<K,V> storage) throws StorageException;
}
