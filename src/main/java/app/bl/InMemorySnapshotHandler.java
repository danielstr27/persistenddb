package app.bl;

import app.exceptions.StorageException;
import app.storage.InMemoryStorage;
import app.storage.PersistentStorage;

public class InMemorySnapshotHandler<K, V> implements SnapshotHandler<K, V> {

    @Override
    public PersistentStorage<K, V> getSnapshot(PersistentStorage<K, V> storage) throws StorageException {
        if (!(storage instanceof InMemoryStorage)) {
            throw new StorageException("Couldn't retrieve snapshot from storage");
        }

        return new InMemoryStorage<K, V>((InMemoryStorage<K, V>) storage);
    }
}
