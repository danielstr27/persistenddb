package app.bl;

import app.storage.PersistentStorage;
import lombok.AllArgsConstructor;
import lombok.Data;

@Data
public class Transaction<K,V> {
    private PersistentStorage<K,V> snapshot;

    public Transaction(PersistentStorage<K, V> storage) {
        this.snapshot = storage;
    }
}
