package app.bl;

import app.exceptions.StorageException;
import app.storage.BackendStorage;
import app.storage.InMemoryStorage;
import app.storage.PersistentStorage;

import java.util.ArrayList;
import java.util.List;

public class BackendSnapshotHandler<K, V> implements SnapshotHandler<K, V> {

    private List<K> keys;

    public BackendSnapshotHandler()
    {
        // Assumption it is a pool of all keys
        keys = new ArrayList<>();
    }

    @Override
    public PersistentStorage<K, V> getSnapshot(PersistentStorage<K, V> storage) throws StorageException {
        if (!(storage instanceof BackendStorage)) {
            throw new StorageException("Couldn't retrieve snapshot from storage");
        }

        BackendStorage<K,V> currentStorage = (BackendStorage<K,V>)storage;

        for(K key: keys)
        {
            // TODO return relevant snapshot
            currentStorage.get(key);
        }

        return new BackendStorage<K, V>();
    }
}
