package app.exceptions;

public class TransactionException extends Throwable {
    public TransactionException(String message) {
        super(message);
    }
}
