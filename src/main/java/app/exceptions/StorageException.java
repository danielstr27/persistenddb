package app.exceptions;

public class StorageException extends Throwable {
    public StorageException(String message) {
        super(message);
    }
}
